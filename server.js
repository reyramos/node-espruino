var io = require('socket.io').listen(9998);
var nodeEspruino = require('node-espruino');

var espruino = nodeEspruino.espruino({
				boardSerial: '34ffd705-41563235-15741043'
			});


io.enable('browser client minification');  // send minified client
io.enable('browser client etag');          // apply etag caching logic based on version number
io.enable('browser client gzip');          // gzip the file
io.set('log level', 1);                    // reduce logging
io.set('transports', [                     // enable all transports (optional if you want flashsocket)
      'websocket'
    , 'flashsocket'
    , 'htmlfile'
    , 'xhr-polling'
    , 'jsonp-polling'
]);

io.set('destroy upgrade', true);
io.set('flash policy server', true);

//array to store all clients connected
var clients = [];

io.sockets.on('connection', function (socket) {

	var connection = {
		  id:socket.id
		, socket: socket
		, transport:socket.transport
	}

	//push it to clients array
	clients[connection.id] = connection;
	//keep a record of the instance
	clients.push(socket);

    console.info('SOCKET ID: ' + socket.id);
	socket.on('espruino', function (msg) {
		msg = JSON.parse(msg);
		console.log('espruino', msg.data)
	espruino.open(function(){
			espruino.command(msg.data, function(result){
				console.log(result);
				result.promise = msg.promise
				socket.json.send(result);
			});
		});
	})


    /**
     * Drop the connection from the array, the user has disconnected
     * by dropping page, or network disconnect
     * Drop connection from clients array,
     * Check if client also started to play to drop player
     *
     */
    socket.on('disconnect', function () {
        //remove client from connected clients
        delete clients[connection.id];
    });
});


